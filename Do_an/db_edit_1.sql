USE master
go
drop database QuanLyQuanBeerThoaiNguyen
GO
/****** Object:  Database [QuanLyQuanBeerThoaiNguyen]    Script Date: 17/09/2020 20:13:28 ******/
CREATE DATABASE QuanLyQuanBeerThoaiNguyen
 --CONTAINMENT = NONE
 --ON  PRIMARY 
--( NAME = N'QuanLyQuanBeerThoaiNguyen', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\QuanLyQuanBeerThoaiNguyen.mdf' , SIZE = 3264KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 --LOG ON 
--( NAME = N'QuanLyQuanBeerThoaiNguyen_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\QuanLyQuanBeerThoaiNguyen_log.ldf' , SIZE = 816KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [QuanLyQuanBeerThoaiNguyen].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET ARITHABORT OFF 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET  ENABLE_BROKER 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET RECOVERY FULL 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET  MULTI_USER 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET DB_CHAINING OFF 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET DELAYED_DURABILITY = DISABLED 
GO
EXEC sys.sp_db_vardecimal_storage_format N'QuanLyQuanBeerThoaiNguyen', N'ON'
GO
USE [QuanLyQuanBeerThoaiNguyen]
GO
/****** Object:  UserDefinedFunction [dbo].[fuConvertToUnsign1]    Script Date: 17/09/2020 20:13:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[fuConvertToUnsign1]
(
 @strInput NVARCHAR(4000)
)
RETURNS NVARCHAR(4000)
AS
BEGIN 
 IF @strInput IS NULL RETURN @strInput
 IF @strInput = '' RETURN @strInput
 DECLARE @RT NVARCHAR(4000)
 DECLARE @SIGN_CHARS NCHAR(136)
 DECLARE @UNSIGN_CHARS NCHAR (136)
 SET @SIGN_CHARS = N'ăâđêôơưàảãạáằẳẵặắầẩẫậấèẻẽẹéềểễệế
 ìỉĩịíòỏõọóồổỗộốờởỡợớùủũụúừửữựứỳỷỹỵý
 ĂÂĐÊÔƠƯÀẢÃẠÁẰẲẴẶẮẦẨẪẬẤÈẺẼẸÉỀỂỄỆẾÌỈĨỊÍ
 ÒỎÕỌÓỒỔỖỘỐỜỞỠỢỚÙỦŨỤÚỪỬỮỰỨỲỶỸỴÝ'
 +NCHAR(272)+ NCHAR(208)
 SET @UNSIGN_CHARS = N'aadeoouaaaaaaaaaaaaaaaeeeeeeeeee
 iiiiiooooooooooooooouuuuuuuuuuyyyyy
 AADEOOUAAAAAAAAAAAAAAAEEEEEEEEEEIIIII
 OOOOOOOOOOOOOOOUUUUUUUUUUYYYYYDD'
 DECLARE @COUNTER int
 DECLARE @COUNTER1 int
 SET @COUNTER = 1
 WHILE (@COUNTER <=LEN(@strInput))
 BEGIN 
 SET @COUNTER1 = 1
 WHILE (@COUNTER1 <=LEN(@SIGN_CHARS)+1)
 BEGIN
 IF UNICODE(SUBSTRING(@SIGN_CHARS, @COUNTER1,1))
 = UNICODE(SUBSTRING(@strInput,@COUNTER ,1) )
 BEGIN 
 IF @COUNTER=1
 SET @strInput = SUBSTRING(@UNSIGN_CHARS, @COUNTER1,1)
 + SUBSTRING(@strInput, @COUNTER+1,LEN(@strInput)-1) 
 ELSE
 SET @strInput = SUBSTRING(@strInput, 1, @COUNTER-1)
 +SUBSTRING(@UNSIGN_CHARS, @COUNTER1,1)
 + SUBSTRING(@strInput, @COUNTER+1,LEN(@strInput)- @COUNTER)
 BREAK
 END
 SET @COUNTER1 = @COUNTER1 +1
 END
 SET @COUNTER = @COUNTER +1
 END
 SET @strInput = replace(@strInput,' ','-')
 RETURN @strInput
END


GO
/****** Object:  Table [dbo].[Ban]    Script Date: 17/09/2020 20:13:28 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Ban](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TenBan] [nvarchar](100) NOT NULL,
	[TrangThai] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HoaDon]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HoaDon](
	[id] [int] IDENTITY(100,1) NOT NULL,
	[ThoiGianVao] [datetime] NOT NULL,
	[ThoiGianRa] [datetime] NULL,
	[idBan] [int] NOT NULL,
	[TongCong] [float] NULL,
	[TienThua] [float] NULL,
	[KhuyenMai] [float] NULL,
	[VAT] [float] NULL,
	[NhanVien] [nvarchar](200) NULL,
	[Voucher] [varchar](100) NULL,
	[TrangThai] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO

/****** Object:  Table [dbo].[KhuyenMai]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[KhuyenMai](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TenKM] [nvarchar](100) NOT NULL,
	[GiamGia] [int] NOT NULL,
	[GiamTien] [float] NOT NULL,
	[ToiDa] [float] NULL DEFAULT ((0)),
	[DieuKien] [float] NULL DEFAULT ((0)),
	[idSanPham] [int] NULL,
	[idLoaiKM] [int] NOT NULL,
	[TrangThai] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiKhuyenMai]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiKhuyenMai](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiKM] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[LoaiSanPham]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[LoaiSanPham](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TenLoaiSanPham] [nvarchar](100) NOT NULL,
	[DanhMuc] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[SanPham]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SanPham](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[TenSanPham] [nvarchar](100) NOT NULL,
	[DonVi] [nvarchar](100) NOT NULL,
	[idLoai] [int] NOT NULL,
	[Gia] [float] NOT NULL,
	[HinhAnh] [varchar](100) NULL DEFAULT ('Khongcohinhanh.jpg'),
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TaiKhoan]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TaiKhoan](
	[TenDangNhap] [varchar](100) NOT NULL,
	[MatKhau] [nvarchar](100) NOT NULL,
	[LoaiTaiKhoan] [nvarchar](10) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[TenDangNhap] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ThongTinHoaDon]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ThongTinHoaDon](
	[id] [int] IDENTITY(1000,1) NOT NULL,
	[idHoaDon] [int] NOT NULL,
	[idSanPham] [int] NOT NULL,
	[SoLuong] [int] NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ThongTinTaiKhoan]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ThongTinTaiKhoan](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[HoTen] [nvarchar](100) NOT NULL,
	[SoDienThoai] [varchar](100) NOT NULL,
	[DiaChi] [nvarchar](100) NOT NULL,
	[CMND] [varchar](100) NOT NULL,
	[Tuoi] [int] NOT NULL,
	[GioiTinh] [nvarchar](100) NOT NULL,
	[TenDangNhap] [varchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Voucher]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Voucher](
	[id] [varchar](100) NOT NULL,
	[TenVoucher] [nvarchar](100) NOT NULL,
	[GiamGia] [int] NOT NULL,
	[GiamTien] [float] NOT NULL,
	[HanSuDung] [date] NOT NULL,
	[TrangThai] [nvarchar](100) NOT NULL
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[Ban] ON 

INSERT [dbo].[Ban] ([id], [TenBan], [TrangThai]) VALUES (1, N'Bàn 01', N'Trống')
INSERT [dbo].[Ban] ([id], [TenBan], [TrangThai]) VALUES (2, N'Bàn 02', N'Trống')
INSERT [dbo].[Ban] ([id], [TenBan], [TrangThai]) VALUES (3, N'Bàn 03', N'Trống')
INSERT [dbo].[Ban] ([id], [TenBan], [TrangThai]) VALUES (4, N'Bàn 04', N'Trống')
INSERT [dbo].[Ban] ([id], [TenBan], [TrangThai]) VALUES (5, N'Bàn 05', N'Trống')
INSERT [dbo].[Ban] ([id], [TenBan], [TrangThai]) VALUES (6, N'Bàn 06', N'Trống')
INSERT [dbo].[Ban] ([id], [TenBan], [TrangThai]) VALUES (7, N'Bàn 07', N'Trống')
INSERT [dbo].[Ban] ([id], [TenBan], [TrangThai]) VALUES (8, N'Bàn 08', N'Trống')
INSERT [dbo].[Ban] ([id], [TenBan], [TrangThai]) VALUES (9, N'Bàn 09', N'Trống')
INSERT [dbo].[Ban] ([id], [TenBan], [TrangThai]) VALUES (10, N'Bàn 10', N'Trống')
SET IDENTITY_INSERT [dbo].[Ban] OFF
SET IDENTITY_INSERT [dbo].[KhuyenMai] ON 

INSERT [dbo].[KhuyenMai] ([id], [TenKM], [GiamGia], [GiamTien], [ToiDa], [DieuKien], [idSanPham], [idLoaiKM], [TrangThai]) VALUES (1, N'Mừng VN chiến thằng -10% Tổng hóa đơn(tối đa 100k)', 10, 0, 100000, NULL, NULL, 1, N'Đang diễn ra')
INSERT [dbo].[KhuyenMai] ([id], [TenKM], [GiamGia], [GiamTien], [ToiDa], [DieuKien], [idSanPham], [idLoaiKM], [TrangThai]) VALUES (2, N'Noel Ấm Áp - Giảm 100k cho hóa đơn trên 1tr', 0, 100000, 100000, 1000000, NULL, 1, N'Đang diễn ra')
INSERT [dbo].[KhuyenMai] ([id], [TenKM], [GiamGia], [GiamTien], [ToiDa], [DieuKien], [idSanPham], [idLoaiKM], [TrangThai]) VALUES (3, N'Mừng xuân 2020 Giảm giá không giới hạn 5% tổng hóa đơn', 5, 0, NULL, NULL, NULL, 1, N'Đang diễn ra')
INSERT [dbo].[KhuyenMai] ([id], [TenKM], [GiamGia], [GiamTien], [ToiDa], [DieuKien], [idSanPham], [idLoaiKM], [TrangThai]) VALUES (4, N'Giảm 10% Món cá chép om dưa', 10, 0, NULL, NULL, 1, 2, N'Không hoạt động')
SET IDENTITY_INSERT [dbo].[KhuyenMai] OFF
SET IDENTITY_INSERT [dbo].[LoaiKhuyenMai] ON 

INSERT [dbo].[LoaiKhuyenMai] ([id], [TenLoaiKM]) VALUES (1, N'Giảm giá hóa đơn')
INSERT [dbo].[LoaiKhuyenMai] ([id], [TenLoaiKM]) VALUES (2, N'Giảm giá món')
INSERT [dbo].[LoaiKhuyenMai] ([id], [TenLoaiKM]) VALUES (3, N'Tặng món')
INSERT [dbo].[LoaiKhuyenMai] ([id], [TenLoaiKM]) VALUES (4, N'Mua A món B tặng C')
SET IDENTITY_INSERT [dbo].[LoaiKhuyenMai] OFF
SET IDENTITY_INSERT [dbo].[LoaiSanPham] ON 

INSERT [dbo].[LoaiSanPham] ([id], [TenLoaiSanPham], [DanhMuc]) VALUES (1, N'Cá', N'Món ăn')
INSERT [dbo].[LoaiSanPham] ([id], [TenLoaiSanPham], [DanhMuc]) VALUES (2, N'Bò', N'Món ăn')
INSERT [dbo].[LoaiSanPham] ([id], [TenLoaiSanPham], [DanhMuc]) VALUES (3, N'Lợn', N'Món ăn')
INSERT [dbo].[LoaiSanPham] ([id], [TenLoaiSanPham], [DanhMuc]) VALUES (4, N'Beer', N'Đồ uống')
INSERT [dbo].[LoaiSanPham] ([id], [TenLoaiSanPham], [DanhMuc]) VALUES (5, N'Khác', N'Khác')
SET IDENTITY_INSERT [dbo].[LoaiSanPham] OFF
SET IDENTITY_INSERT [dbo].[SanPham] ON 

INSERT [dbo].[SanPham] ([id], [TenSanPham], [DonVi], [idLoai], [Gia], [HinhAnh]) VALUES (1, N'Cá chép om dưa', N'Đĩa', 1, 88888, N'Cachepomdua.jpg')
INSERT [dbo].[SanPham] ([id], [TenSanPham], [DonVi], [idLoai], [Gia], [HinhAnh]) VALUES (2, N'Cá diêu hồng hai món', N'Đĩa', 1, 99999, N'Cadieuhonghaimon.jpg')
INSERT [dbo].[SanPham] ([id], [TenSanPham], [DonVi], [idLoai], [Gia], [HinhAnh]) VALUES (3, N'Bò xiên nướng', N'Xiên', 2, 100000, N'Boxiennuong.jpg')
INSERT [dbo].[SanPham] ([id], [TenSanPham], [DonVi], [idLoai], [Gia], [HinhAnh]) VALUES (4, N'Bò lúc lắc khoai tây', N'Đĩa', 2, 111111, N'Boluclackhoaitay.jpg')
INSERT [dbo].[SanPham] ([id], [TenSanPham], [DonVi], [idLoai], [Gia], [HinhAnh]) VALUES (5, N'Chân giò hun khói', N'Phần', 3, 87000, N'Changiohunkhoi.jpg')
INSERT [dbo].[SanPham] ([id], [TenSanPham], [DonVi], [idLoai], [Gia], [HinhAnh]) VALUES (6, N'Chân giò hầm nấm', N'Phần', 3, 96000, N'Changiohamnam.jpg')
INSERT [dbo].[SanPham] ([id], [TenSanPham], [DonVi], [idLoai], [Gia], [HinhAnh]) VALUES (7, N'Sài gòn xanh', N'Chai', 4, 15000, N'Saigonxanh.jpg')
INSERT [dbo].[SanPham] ([id], [TenSanPham], [DonVi], [idLoai], [Gia], [HinhAnh]) VALUES (8, N'Tiger nâu', N'Chai', 4, 18000, N'Tigernau.jpg')
SET IDENTITY_INSERT [dbo].[SanPham] OFF
INSERT [dbo].[TaiKhoan] ([TenDangNhap], [MatKhau], [LoaiTaiKhoan]) VALUES (N'1', N'1', N'Nhân viên')
INSERT [dbo].[TaiKhoan] ([TenDangNhap], [MatKhau], [LoaiTaiKhoan]) VALUES (N'admin', N'1', N'Quản lý')
INSERT [dbo].[TaiKhoan] ([TenDangNhap], [MatKhau], [LoaiTaiKhoan]) VALUES (N'manager', N'1', N'Quản lý')
INSERT [dbo].[TaiKhoan] ([TenDangNhap], [MatKhau], [LoaiTaiKhoan]) VALUES (N'staff', N'1', N'Nhân viên')
INSERT [dbo].[TaiKhoan] ([TenDangNhap], [MatKhau], [LoaiTaiKhoan]) VALUES (N'thukho', N'1', N'Thủ kho')
SET IDENTITY_INSERT [dbo].[ThongTinTaiKhoan] ON 

INSERT [dbo].[ThongTinTaiKhoan] ([id], [HoTen], [SoDienThoai], [DiaChi], [CMND], [Tuoi], [GioiTinh], [TenDangNhap]) VALUES (1, N'Ten', N'123456798', N'Dia Chi', N'123456', 20, N'Nam', N'admin')
INSERT [dbo].[ThongTinTaiKhoan] ([id], [HoTen], [SoDienThoai], [DiaChi], [CMND], [Tuoi], [GioiTinh], [TenDangNhap]) VALUES (2, N'Việt Phi', N'123456798', N'Quận 9', N'251120116', 20, N'Nam', N'manager')
INSERT [dbo].[ThongTinTaiKhoan] ([id], [HoTen], [SoDienThoai], [DiaChi], [CMND], [Tuoi], [GioiTinh], [TenDangNhap]) VALUES (3, N'Cao Ngọc Nguyên', N'123456798', N'Quận 10', N'251120115', 20, N'Nam', N'staff')
INSERT [dbo].[ThongTinTaiKhoan] ([id], [HoTen], [SoDienThoai], [DiaChi], [CMND], [Tuoi], [GioiTinh], [TenDangNhap]) VALUES (6, N'123', N'123', N'12312', N'123', 19, N'Nam', N'1')
SET IDENTITY_INSERT [dbo].[ThongTinTaiKhoan] OFF
INSERT [dbo].[Voucher] ([id], [TenVoucher], [GiamGia], [GiamTien], [HanSuDung], [TrangThai]) VALUES (N'SVIP', N'Giảm 20 % Tổng hóa đơn', 20, 0, CAST(N'2022-10-29' AS Date), N'Đã sử dụng')
INSERT [dbo].[Voucher] ([id], [TenVoucher], [GiamGia], [GiamTien], [HanSuDung], [TrangThai]) VALUES (N'ADMINVIPPRO', N'Vì anh là ADMIN sao lại phải mất tiền', 100, 0, CAST(N'2022-10-29' AS Date), N'Chưa sử dụng')
INSERT [dbo].[Voucher] ([id], [TenVoucher], [GiamGia], [GiamTien], [HanSuDung], [TrangThai]) VALUES (N'SVIP202020', N'Giảm 20 % Tổng hóa đơn', 20, 0, CAST(N'2022-10-29' AS Date), N'Chưa sử dụng')
INSERT [dbo].[Voucher] ([id], [TenVoucher], [GiamGia], [GiamTien], [HanSuDung], [TrangThai]) VALUES (N'VIP200', N'Giảm 200.000 VND Tổng hóa đơn', 0, 200000, CAST(N'2022-10-29' AS Date), N'Chưa sử dụng')
ALTER TABLE [dbo].[HoaDon] ADD  DEFAULT ((0)) FOR [TongCong]
GO
ALTER TABLE [dbo].[HoaDon] ADD  DEFAULT ((0)) FOR [TienThua]
GO
ALTER TABLE [dbo].[HoaDon] ADD  DEFAULT ((0)) FOR [KhuyenMai]
GO
ALTER TABLE [dbo].[HoaDon] ADD  DEFAULT ((0)) FOR [VAT]
GO
ALTER TABLE [dbo].[ThongTinHoaDon] ADD  DEFAULT ((0)) FOR [SoLuong]
GO
ALTER TABLE [dbo].[HoaDon]  WITH CHECK ADD FOREIGN KEY([idBan])
REFERENCES [dbo].[Ban] ([id])
GO
ALTER TABLE [dbo].[KhuyenMai]  WITH CHECK ADD FOREIGN KEY([idLoaiKM])
REFERENCES [dbo].[LoaiKhuyenMai] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[KhuyenMai]  WITH CHECK ADD FOREIGN KEY([idSanPham])
REFERENCES [dbo].[SanPham] ([id])
GO
ALTER TABLE [dbo].[SanPham]  WITH CHECK ADD FOREIGN KEY([idLoai])
REFERENCES [dbo].[LoaiSanPham] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ThongTinHoaDon]  WITH CHECK ADD FOREIGN KEY([idSanPham])
REFERENCES [dbo].[SanPham] ([id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ThongTinHoaDon]  WITH CHECK ADD FOREIGN KEY([idHoaDon])
REFERENCES [dbo].[HoaDon] ([id])
GO
ALTER TABLE [dbo].[ThongTinTaiKhoan]  WITH CHECK ADD FOREIGN KEY([TenDangNhap])
REFERENCES [dbo].[TaiKhoan] ([TenDangNhap])
ON DELETE CASCADE
GO
/****** Object:  StoredProcedure [dbo].[ChuyenNMon]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[ChuyenNMon]
@idHoaDon INT,@idSanPham INT,@idTableTo INT,@idTableFrom INT
AS
BEGIN
	SELECT * 
	INTO TEMP
	FROM ThongTinHoaDon
	WHERE idHoaDon = @idHoaDon and idSanPham = @idSanPham;

	declare @idBillTo int
	declare @voucher Varchar(100)

	SELECT @voucher = Voucher from HoaDon Where idBan = @idTableFrom and TrangThai = N'Chưa thanh toán'

	SELECT @idBillTo = id from HoaDon Where idBan = @idTableTo and TrangThai = N'Chưa thanh toán'

	if (@idBillTo is null)
	begin
		INSERT INTO HoaDon(ThoiGianVao,idBan,TrangThai,TongCong)
		VALUES(GetDate(),@idTableTo,N'Chưa thanh toán',0)
		SELECT @idBillTo = Max(id) FROM HoaDon WHERE idBan = @idTableTo and TrangThai = N'Chưa thanh toán'
	end
	if (@voucher is not null)
	begin
		UPDATE HoaDon SET Voucher = @voucher Where id =@idBillTo
	end

	UPDATE ThongTinHoaDon SET idHoaDon = @idBillTo WHERE ID IN (SELECT ID from TEMP)
	
	DROP table dbo.TEMP
END

GO
/****** Object:  StoredProcedure [dbo].[themBan]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[themBan]
@tenBan nvarchar(100)
AS
BEGIN
	INSERT INTO dbo.Ban(TenBan,TrangThai)
	VALUES(@tenBan,N'Trống')
END

GO
/****** Object:  StoredProcedure [dbo].[themLoaiSanPham]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[themLoaiSanPham]
@tenLoaiSanPham nvarchar(100),@danhMuc nvarchar(100)
AS
BEGIN
	INSERT INTO dbo.LoaiSanPham(TenLoaiSanPham,DanhMuc)
	VALUES(@tenLoaiSanPham, @danhMuc )
END

GO
/****** Object:  StoredProcedure [dbo].[themTaiKhoan]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[themTaiKhoan]
@ten varchar(100), @matKhau nvarchar(100), @loai nvarchar(10)
AS
BEGIN
	INSERT INTO dbo.TaiKhoan( TenDangNhap, MatKhau, LoaiTaiKhoan )
	VALUES (@ten, @matKhau, @loai)
END

GO
/****** Object:  StoredProcedure [dbo].[themThongTinTk]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROC [dbo].[themThongTinTk]
@tenDangNhap varchar(100), @hoTen nvarchar(100), @diaChi nvarchar(100), @cMND VARCHAR(100), @sDT VARCHAR(100), @tuoi INT, @gioiTinh nvarchar(100)
AS
BEGIN
	INSERT INTO dbo.ThongTinTaiKhoan(HoTen, SoDienThoai, DiaChi, CMND, Tuoi, GioiTinh, TenDangNhap)
	VALUES
	( @hoTen,  @sDT, @diaChi, @cMND, @tuoi, @gioiTinh,  @tenDangNhap )
END

GO
/****** Object:  StoredProcedure [dbo].[USP_BCTK]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
--- Bao cao thong ke
Create Proc [dbo].[USP_BCTK]
@fromDate DateTime,@toDate DateTime
as
begin
	select hd.id,b.TenBan,hd.TongCong,NhanVien 
	from HoaDon hd,Ban b
	WHERE hd.idBan = b.id  AND ThoiGianRa BETWEEN @fromDate and @toDate and hd.TrangThai = N'Đã thanh toán'
end

GO
/****** Object:  StoredProcedure [dbo].[USP_DangNhap]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[USP_DangNhap]
@TenDangNhap NVARCHAR(100), @MatKhau NVARCHAR(100)
AS
BEGIN
	SELECT * FROM dbo.TaiKhoan WHERE TenDangNhap=@TenDangNhap AND MatKhau =@MatKhau 
END

GO
/****** Object:  StoredProcedure [dbo].[USP_DeleteSP]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create Proc [dbo].[USP_DeleteSP]
@idHoaDon INT, @idSanPham INT
AS
BEGIN
	DELETE dbo.ThongTinHoaDon Where idHoaDon = @idHoaDon AND idSanPham = @idSanPham
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetDsKho]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
/****** Object:  StoredProcedure [dbo].[USP_GetDSKM]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROC [dbo].[USP_GetDSKM]
AS
BEGIN
	SELECT * FROM KhuyenMai Where TrangThai = N'Đang diễn ra'
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetDSKMAll]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROC [dbo].[USP_GetDSKMAll]
AS
BEGIN
	SELECT * FROM KhuyenMai 
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetHoaDon]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create PROC [dbo].[USP_GetHoaDon]
@idBan INT
AS
BEGIN
	SELECT f.TenSanPham,bi.SoLuong ,f.Gia,f.DonVi,f.Gia*bi.SoLuong AS ThanhTien 
	FROM dbo.ThongTinHoaDon AS bi ,dbo.HoaDon AS b,dbo.SanPham AS f 
	WHERE bi.idHoaDon = b.id AND bi.idSanPham = f.id AND b.TrangThai=N'Chưa thanh toán' AND b.idBan =@idBan
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetListDoUong]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[USP_GetListDoUong]
AS
BEGIN
	SELECT * 
	FROM SanPham a, LoaiSanPham b
	WHERE a.idLoai = b.id AND b.DanhMuc = N'Đồ uống'
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetListFood]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[USP_GetListFood]
AS
BEGIN
	SELECT * FROM SanPham
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetListKhac]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[USP_GetListKhac]
AS
BEGIN
	SELECT * 
	FROM SanPham a, LoaiSanPham b
	WHERE a.idLoai = b.id AND b.DanhMuc = N'Khác'
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetListMonAn]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[USP_GetListMonAn]
AS
BEGIN
	SELECT * 
	FROM SanPham a, LoaiSanPham b
	WHERE a.idLoai = b.id AND b.DanhMuc = N'Món ăn'
END

GO
/****** Object:  StoredProcedure [dbo].[USP_GetTableList]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[USP_GetTableList]
AS
BEGIN
	SELECT * FROM Ban
END

GO
/****** Object:  StoredProcedure [dbo].[USP_InBill]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[USP_InBill]
@idBan INT
AS
BEGIN
	SELECT ROW_NUMBER() OVER (ORDER BY TenSanPham) AS [STT] ,f.TenSanPham ,bi.SoLuong,f.Gia ,f.Gia*bi.SoLuong AS ThanhTien
	FROM dbo.ThongTinHoaDon AS bi ,dbo.HoaDon AS b,dbo.SanPham AS f 
	WHERE bi.idHoaDon = b.id AND bi.idSanPham = f.id AND b.TrangThai=N'Chưa thanh toán' AND b.idBan = @idBan
END

GO
/****** Object:  StoredProcedure [dbo].[USP_InBill1]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROC [dbo].[USP_InBill1]
@idHoaDon INT
AS
BEGIN
	SELECT ROW_NUMBER() OVER (ORDER BY TenSanPham) AS [STT] ,f.TenSanPham ,bi.SoLuong,f.Gia ,f.Gia*bi.SoLuong AS ThanhTien
	FROM dbo.ThongTinHoaDon AS bi ,dbo.HoaDon AS b,dbo.SanPham AS f 
	WHERE bi.idHoaDon = b.id AND bi.idSanPham = f.id AND b.TrangThai=N'Đã thanh toán' AND bi.idHoaDon = @idHoaDon
END

GO
/****** Object:  StoredProcedure [dbo].[USP_InsertBill]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create PROC [dbo].[USP_InsertBill]
@idBan INT,
@TongCong FLOAT
AS
BEGIN
	INSERT INTO dbo.HoaDon (ThoiGianVao, ThoiGianRa, idBan, TongCong, TrangThai) 
	VALUES ( GETDATE(), null, @idBan, @TongCong , N'Chưa thanh toán') 
END

GO
/****** Object:  StoredProcedure [dbo].[USP_InsertBillInfo]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[USP_InsertBillInfo]
@idHoaDon INT,@idSanPham INT , @SoLuong INT
AS
BEGIN

	DECLARE @isExitsBillInfo INT
	DECLARE @foodCount INT = 1
	--DECLARE @SumSL INT = 0

	SELECT @isExitsBillInfo = id,@foodCount = b.SoLuong 
	FROM dbo.ThongTinHoaDon AS b 
	WHERE idHoaDon = @idHoaDon AND idSanPham = @idSanPham

	IF(@isExitsBillInfo >0)
		BEGIN
			DECLARE @newCount INT = @foodCount + @SoLuong
			IF (@newCount > 0)
				UPDATE dbo.ThongTinHoaDon SET SoLuong = @newCount WHERE idSanPham = @idSanPham
			ELSE
				DELETE dbo.ThongTinHoaDon WHERE idHoaDon = @idHoaDon AND idSanPham = @idSanPham
		END
	ELSE 
		BEGIN
			DECLARE @Count INT = @SoLuong
			IF (@Count > 0)
				BEGIN
					INSERT INTO dbo.ThongTinHoaDon
						(idHoaDon,idSanPham,SoLuong )
					VALUES
						( @idHoaDon,  @idSanPham,  @SoLuong)
				END
		END
END

GO
/****** Object:  StoredProcedure [dbo].[USP_InsertDiscount]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[USP_InsertDiscount]
@tenKM NVARCHAR(200) , @giamGia INT , @giamTien FLOAT, @toiDa FLOAT, @dieuKien FLOAT,@idLoaiKM INT
as  
begin
	Insert into dbo.KhuyenMai
	(TenKM,GiamGia,GiamTien,ToiDa,DieuKien,idLoaiKM,TrangThai)
	VALUES
	(@tenKM , @giamGia , @giamTien , @toiDa , @dieuKien,@idLoaiKM,N'Đang diễn ra')
end

GO
/****** Object:  StoredProcedure [dbo].[USP_InsertDrink]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[USP_InsertDrink]
@TenSanPham nvarchar(100),@DonVi nvarchar(100), @Gia float, @idLoai int, @tenAnh varchar(100)
as  
begin
	Insert into dbo.SanPham
	(TenSanPham,DonVi,Gia,idLoai,HinhAnh)
	VALUES
	(@TenSanPham,@DonVi, @Gia, @idLoai,@tenAnh )
end

GO
/****** Object:  StoredProcedure [dbo].[USP_InsertDrink1]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE proc [dbo].[USP_InsertDrink1]
@TenSanPham nvarchar(100),@DonVi nvarchar(100), @Gia float, @idLoai int
as  
begin
	Insert into dbo.SanPham
	(TenSanPham,DonVi,Gia,idLoai)
	VALUES
	(@TenSanPham, @DonVi,@Gia, @idLoai )
end

GO
/****** Object:  StoredProcedure [dbo].[USP_InsertVoucher]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create proc [dbo].[USP_InsertVoucher]
@id varchar(100), @TenVoucher Nvarchar(100), @giamGia INT, @giamTien Float, @hanSuDung Date
as
BEGIN
	INSERT INTO Voucher(id,TenVoucher,GiamGia,GiamTien,HanSuDung,TrangThai)
	VALUES(@id,@TenVoucher,@giamGia,@giamTien,@hanSuDung,N'Chưa sử dụng')
END 

GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateDiscount]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

create proc [dbo].[USP_UpdateDiscount]
@tenKM NVARCHAR(200) , @giamGia INT , @giamTien FLOAT, @toiDa FLOAT, @dieuKien FLOAT,@idLoaiKM INT,@idKM INT
as  
begin
	UPDATE dbo.KhuyenMai 
	SET TenKM = @tenKM,GiamGia = @giamGia,GiamTien = @giamTien,ToiDa = @toiDa ,DieuKien = @dieuKien ,idLoaiKM = @idLoaiKM
	WHERE id = @idKM
end

GO
/****** Object:  StoredProcedure [dbo].[USP_UpdateSL]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROC [dbo].[USP_UpdateSL]
@SoLuong INT,
@idHoaDon INT,
@idSanPham INT
AS
BEGIN
	UPDATE dbo.ThongTinHoaDon SET SoLuong = @SoLuong WHERE  idHoaDon = @idHoaDon AND idSanPham = @idSanPham
END

GO
/****** Object:  StoredProcedure [dbo].[XoaBan]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[XoaBan]
@tenBan NVARCHAR(100)
as
BEGIN
	DELETE dbo.Ban WHERE TenBan = 11
END

GO
/****** Object:  StoredProcedure [dbo].[XoaSp]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROC [dbo].[XoaSp]
@TenSanPham NVARCHAR(100)
as
BEGIN
	DELETE dbo.SanPham WHERE TenSanPham = @TenSanPham
END

GO
/****** Object:  StoredProcedure [dbo].[XoaTK]    Script Date: 17/09/2020 20:13:29 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO



CREATE PROC [dbo].[XoaTK]
@tenDn VARCHAR(100)
AS
BEGIN
	DELETE dbo.ThongTinTaiKhoan WHERE TenDangNhap = @tenDn
	DELETE dbo.TaiKhoan WHERE TenDangNhap = @tenDn 
END

GO
USE [master]
GO
ALTER DATABASE [QuanLyQuanBeerThoaiNguyen] SET  READ_WRITE 
GO
